<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', 'HomeController@index');// base


//get all and create new
Route::get('/ads', 'AdController@index');//get all ads
Route::post('/ads', 'AdController@store');//create new ad

Route::get('/ads/{ad}', 'AdController@show');//get ad with id
Route::put('/ads/{ad}', 'AdController@edit');//edit
Route::delete('/ads/{ad}', 'AdController@destroy');//delete
