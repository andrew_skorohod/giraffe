<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Ad;

class AdController extends Controller
{
    //show all
    public function index()
    {

        $ads = Ad::with('user')->get();
        return $ads->toJson();
    }

    //save
    public function store(Request $request)
    {
        $ad = new Ad;
        $ad->title = $request->title;
        $ad->description = $request->description;
        $ad->user_id = $request->user_id;
        try{
          $ad->save();
          return $ad->toJson();
        }
        catch(Exception $e) {
          return $e;
        }
    }

    //details
    public function show(Request $request, $id)
    {
        return AdController::getObject($id)->toJson();
    }

    public function edit(Request $request, $id)
    {
        $ad = AdController::getObject($id);
        $ad->title = $request->title;
        $ad->description = $request->description;
        $ad->save();
        return $ad->toJson();

    }

    //delete
    public function destroy(Request $request, $id)
    {
        $ad = AdController::getObject($id);
        $ad->delete();
        return $ad->toJson();

    }

    //request to db
    public function getObject($id)
    {
      $obj = Ad::where('id',$id)->first();
      return $obj;
    }
}
