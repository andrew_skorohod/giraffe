<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Ad extends Model
{
    protected $fillable = ['name','description','user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
