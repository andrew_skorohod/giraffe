app.config(function($routeProvider,$locationProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "/public/ang_templates/list.html",
            controller: "ListController",
            resolve: {
                objects: function(Objects) {
                    return Objects.getObjects();
                }
            }
        })
        .when("/new/object", {
            controller: "NewObjectController",
            templateUrl: "/public/ang_templates/form.html"
        })
        .when("/object/:objectId", {
            controller: "EditObjectController",
            templateUrl: "/public/ang_templates/details.html"
        })
        .otherwise({
            redirectTo: "/"
        })
})
