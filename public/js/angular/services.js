app.service("Objects", function($http) {
    this.getObjects = function() {//get all
        return $http.get("/ads").
            then(function(response) {
                return response;
            }, function(response) {
                alert("Error finding contacts.");
            });
    }

    this.createObject = function(object) {//create one
    return $http.post("/ads", object).
        then(function(response) {
            return response;
        }, function(response) {
            alert("Error creating contact.");
        });
    }

    this.getObject = function(objectId) {//get one
    var url = "/ads/" + objectId+"/";
    return $http.get(url).
        then(function(response) {
            return response;
        }, function(response) {
            alert("Error finding this contact.");
        });
    }

    this.editObject = function(object) {//edit
    var url = "/ads/" + object.id+"/";
    return $http.put(url, object).
        then(function(response) {
            return response;
        }, function(response) {
            alert("Error editing this contact.");
        });
    }

    this.deleteObject = function(objectId) {
    var url = "/ads/" + objectId+"/";
    return $http.delete(url).
        then(function(response) {
            return response;
        }, function(response) {
            alert("Error deleting this contact.");
        });
    }



});
