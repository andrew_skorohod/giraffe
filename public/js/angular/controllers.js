app.controller('ListController', function(objects,$scope) {
  $scope.objects = objects.data;
  $scope.user_id = user();
  //start of pagination
  $scope.totalItems = $scope.objects.length;
  $scope.currentPage = 1;
  $scope.numPerPage = 5;

  $scope.paginate = function(value) {
    var begin, end, index;
    begin = ($scope.currentPage - 1) * $scope.numPerPage;
    end = begin + $scope.numPerPage;
    index = $scope.objects.indexOf(value);
    return (begin <= index && index < end);
  };
});

app.controller("NewObjectController", function($scope, $location, Objects) {
    $scope.back = function() {
        $location.path("#/");
    }

    $scope.saveObject = function(object) {
        object.user_id = user();
        Objects.createObject(object).then(function(doc) {
            var contactUrl = "/object/" + doc.data.id;
            $location.path(contactUrl);
        }, function(response) {
            alert(response);
        });
    }
});

app.controller("EditObjectController", function($scope, $routeParams,$location,$timeout, Objects) {
    Objects.getObject($routeParams.objectId).then(function(doc) {
        $scope.object = doc.data;
    }, function(response) {
        alert(response);
    });

    $scope.user_id = user();
    $scope.toggleEdit = function() {
        $scope.editMode = true;
        $scope.objectFormUrl = "/public/ang_templates/form.html";
    }

    $scope.back = function() {
        $scope.editMode = false;
        $scope.objectFormUrl = "";
    }

    $scope.saveObject = function(object) {
        Objects.editObject(object);
        $scope.editMode = false;
        $scope.objectFormUrl = "";
    }

    $scope.deleteObject = function(objectId) {
        Objects.deleteObject(objectId);
        $timeout(function() {//set timeout to prevent reloading of page before deleting
          $location.path("#/")
        }, 500)
    }

});
