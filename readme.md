## Тестовое задание сервис объявлений.
 Ссылка на задание https://docs.google.com/document/d/1mwz7IyN5u5ZCnzbJz6l-a1LDMLRyqLAAArzdtH5kpUQ/edit

# Что использовал
 Для бекенда использовал фреймворк Laravel 5.4. Для фронтенда использовал AngularJS. И немного Bootstrap.
 
# Описание
 Одностраничное (почти) CRUD приложение для работы с объявлениями. Зарегистрированные пользователи создают, редактируют, удаляют свои объявления.
 Незарегистрированые могут просматривать.
# Установка
 
	composer install

	php artisan migrate

	php artisan serve
	
	Для запуска на локалхосте отрадактировать данные 'database' ,'username','password' в файле /config/database.php. Создать таблицу.